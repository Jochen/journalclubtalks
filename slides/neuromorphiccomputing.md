
<!-- .slide: data-background="./static/images/fiber_titlebg.png" -->

# Journal Club 
## 17/10/2021

![title](./images/neurom_title.png)


---

## What is this about?

* Review article 
* use photonics to acclerate AI and ML 
* photonic computing in new disguise?

---

## What is neuromorphic computing

* use large-scale integration with (electronic) analog circuits to mimic neuro-biological architectures
* originates in electronics
* particularly to design hardware to more closely match AI or ML algorithms (massively distributed)


----

## Why?

<div class="flex justify-center"

![why](./images/neurom_why.png)

</div>

----

## Why photonics?


<div class="flex justify-center"

![why photonics](./images/neurom_why_photonics.png)

</div>

---

## Content

- Survey of functions 
- Survey of implementation of each
- Requirements of a neuromorphic photonic processor

---

## Functions

![functions](./images/neurom_functions.png)

---

## Weight addition (photonic synapse)

Essentially does matrix multiplication

Two approaches:

1. Wavelength
2. Modes 

----

![weight addtion](./images/neurom_synapses.png)

---

## Photonic neurons

A neuron needs weight addition + nonlinearity

Two approaches:

1. OEO  
    - can be bandwidth limited
    - needs electronic components
2. Optical NL 
    - weak
    - needs regeneration

----

![neuron](./images/neurom_neuron.png)

----

## Spiking networks

Special case of neurons (in cavity)

![spiking](./images/neurom_spiking.png)


---

## Networks

![networks](./images/neurom_networks.png)



---

## Neuromorphic photonic processor

![processor](./images/neurom_processor.png)


----

## What is needed

- on-chip electronics
   - challenge large number of electronic ports (scales $n^2$ with optical ports)
- optical memory (?)
- on-chip light sources
- stabilisation of photonic devices
